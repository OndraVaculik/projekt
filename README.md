# Projekt
Projekt ještě není určen pro klasifikaci.

Budík vyrobený pomocí STM-8 a sedmisegmentového displeje s MAX7219.

![Fotka projektu](pic/foto.png)

#### MAX7219

Je to integrovaný obvod schopný řídit vícemístné číslicové nebo maticové LED displeje zapojené se společnou katodou. Ovládá se sériovým digitálním rozhraním a je schopen řídit až osmimístné displeje (celkem 8x8 LED) pomocí 16 výstupů. Buzení LED probíhá technikou zvanou multiplexování, takže v jednom okamžiku je rozsvícena vždy jen jedna číslice (celý proces ale běží tak rychle že se lidskému oku zdá jako by svítil celý displej zároveň). Displej v sobě obsahuje i "znakovou" sadu(= má zabudované, jak vypadají všechny symboly a nemusíme mu říkat, ze kterých segmentů se skládají jednotlivá čísla či písmena). Budič umí řídit i jas displeje a lze ho "kaskádovat" (tedy zapojovat za sebe) a vytvářet tak větší displeje. Moduly displejů, které obsahují tento budič se dají levně koupit, jak ve variantě s číslicovým displejem, tak ve variantě s maticovým displejem. Ve svých projektech se přirozeně nemusíte spoléhat jen na tyto dva moduly, ale můžete si zapojit budič s jakýmkoli displejem který se vám líbí. Obvod pracuje s 5V napájecím napětím. Více informací naleznete [zde](http://www.elektromys.eu/clanky/stm8_8_max7219/clanek.html), nebo [zde](https://datasheets.maximintegrated.com/en/ds/MAX7219-MAX7221.pdf).

![Osmimístný sedmisegmentový displej s MAX7219](pic/displej.jpg)

#### STM8S208 Nucleo-64

Deska STM8S208RBT6 Nucleo-64 (NUCLEO-8S208RB) poskytuje cenově dostupný a flexibilní způsob, jak mohou uživatelé vyzkoušet nové koncepty a stavět prototypy s řadou STM8S. Podporuje připojení Arduino™ Uno V3 a morfo hlavičky ST umožňují snadné propojení s dalšímy moduly. Deska Nucleo-64 integruje ST-LINK/V2-1 pro komunikaci, debugger a programátor.
Deska obsahuje jádro STM8 umožňující práci na 24MHz, 128Kb paměti flash a 6Kb paměti RAM.Více informací naleznete [zde](http://www.elektromys.eu/clanky/stm8_1_nucleo/clanek.html), nebo [zde](https://www.st.com/resource/en/user_manual/dm00477617-stm8s208rbt6-nucleo64-board-stmicroelectronics.pdf).

![Deska STM8S208 Nucleo-64](pic/nucleo.jpg)

## Použité součástky

- STM8
- [7-segmentový displej s budičem MAX7219](https://dratek.cz/arduino/3182-led-displej-7-segmentovy-8-znaku-max7219-cerveny.html?addToFavorit)
- [tlačítka](https://dratek.cz/arduino/7720-modul-klavesnice-matice-2-x-2-tlacitka.html)
- [bzučák](https://dratek.cz/arduino/121937-aktivni-bzucak-5v-2.3-khz-2.html)

## Blokové schéma

```mermaid
flowchart LR
PWR[Napájení PC] ===+5V===> MCU[STM8]
MCU--->Displej
BTN[Tlačítka]--->MCU
PWR[Čas PC] ===UART===> MCU[STM8]
```

## Schéma v KiCad

![Schéma](pic/Schema.PNG)

