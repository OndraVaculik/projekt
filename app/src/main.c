#include "stm8s.h"

#define hodiny				10
#define minuty				15
#define sekundy				0

#define CLK_GPIO 			GPIOD
#define CLK_PIN  			GPIO_PIN_4
#define DATA_GPIO 			GPIOD
#define DATA_PIN  			GPIO_PIN_3
#define CS_GPIO 			GPIOD
#define CS_PIN  			GPIO_PIN_2

#define CLK_HIGH 			GPIO_WriteHigh(CLK_GPIO, CLK_PIN)
#define CLK_LOW 			GPIO_WriteLow(CLK_GPIO, CLK_PIN)
#define DATA_HIGH 			GPIO_WriteHigh(DATA_GPIO, DATA_PIN)
#define DATA_LOW 			GPIO_WriteLow(DATA_GPIO, DATA_PIN)
#define CS_HIGH 			GPIO_WriteHigh(CS_GPIO, CS_PIN)
#define CS_LOW 				GPIO_WriteLow(CS_GPIO, CS_PIN)

#define NOOP 				0
#define DIGIT0 				1
#define DIGIT1 				2
#define DIGIT2 				3
#define DIGIT3 				4
#define DIGIT4 				5
#define DIGIT5 				6
#define DIGIT6 				7
#define DIGIT7 				8
#define DECODE_MODE 		9
#define INTENSITY 			10
#define SCAN_LIMIT 			11
#define SHUTDOWN 			12
#define DISPLAY_TEST 		15

#define DISPLAY_ON			1
#define DISPLAY_OFF			0
#define DISPLAY_TEST_ON 	1
#define DISPLAY_TEST_OFF 	0
#define DECODE_ALL			0b11111111
#define DECODE_NONE			0

void max7219_init(void);
void max7219_posli(uint8_t adresa, uint8_t data);
void delay_s(void);
uint8_t S1 (void);
uint8_t S2 (void);
void send_char (char c);
void send_string (char str[]);
char get_char ();

uint8_t button_1 (void)
{
    return (GPIO_ReadInputPin(GPIOG, GPIO_PIN_4)==0);
}

uint8_t button_2 (void)
{
    return (GPIO_ReadInputPin(GPIOG, GPIO_PIN_5)==0);
}

void main(void){
	max7219_init();
	GPIO_Init(GPIOG,GPIO_PIN_4,GPIO_MODE_IN_FL_NO_IT);
	GPIO_Init(GPIOG,GPIO_PIN_5,GPIO_MODE_IN_FL_NO_IT);
	GPIO_Init(GPIOD,GPIO_PIN_0,GPIO_MODE_OUT_OD_LOW_SLOW);
	int show = 1;

    UART1_Init(
        9600,
        UART1_WORDLENGTH_8D,
        UART1_STOPBITS_1,
        UART1_PARITY_NO,
        UART1_SYNCMODE_CLOCK_DISABLE,
        UART1_MODE_TXRX_ENABLE);

    UART1_Cmd(ENABLE);
    int hod = 0;
    int min = 0;
    int sec = 0;
	char data[5];

    while (1)
    {

        for(int i=0;i<5;i++){
            data[i] = get_char(); 
        }
        if(data[0]==0xFF){
            if(data[1]==0xEE){
                hod = data[2];
                min = data[3];
                sec = data[4];
               }
        }
        else
        {
            for(int i=0;i<5;i++){
                data[i] = 0; 
            }
        }
        for (uint32_t i = 0; i < 50000; i++)
            ;
        
    }

    if ( button_1())
	{
		GPIO_WriteLow(GPIOD,GPIO_PIN_0);
	}

	while ( button_2())
	{
		show=0;
		int x=(int)hodiny/10;
		int y=(int)minuty/10;
		int z=(int)sekundy/10;
		max7219_posli(DIGIT7,x);
		max7219_posli(DIGIT6,hodiny%10);
		max7219_posli(DIGIT5,0b11111111);
		max7219_posli(DIGIT4,y);
		max7219_posli(DIGIT3,minuty%10);
		max7219_posli(DIGIT2,0b11111111);
		max7219_posli(DIGIT1,z);
		max7219_posli(DIGIT0,sekundy%10);
	}

	if(hod==hodiny){
		if(min==minuty){
			if(sec==sekundy){
				GPIO_WriteHigh(GPIOD,GPIO_PIN_0);
			}
		}
	}

	while(show){
		int a=(int)hod/10;
		int b=(int)min/10;
		int c=(int)sec/10;
		max7219_posli(DIGIT7,a);
		max7219_posli(DIGIT6,hod%10);
		max7219_posli(DIGIT5,0b11111111);
		max7219_posli(DIGIT4,b);
		max7219_posli(DIGIT3,min%10);
		max7219_posli(DIGIT2,0b11111111);
		max7219_posli(DIGIT1,c);
		max7219_posli(DIGIT0,sec%10);
	}
}

void send_char (char c)
{
    while(!UART1_GetFlagStatus(UART1_FLAG_TXE))
        ;
    UART1_SendData8(c);
}

void send_string (char str[])
{
    for (uint32_t i=0; str[i];i++)
        send_char(str[i]);
}

char get_char (void)
{
    while (!UART1_GetFlagStatus(UART1_FLAG_RXNE))
        ;
    return UART1_ReceiveData8();
}

void max7219_init(void){
GPIO_Init(CS_GPIO,CS_PIN,GPIO_MODE_OUT_PP_LOW_SLOW);
GPIO_Init(CLK_GPIO,CLK_PIN,GPIO_MODE_OUT_PP_LOW_SLOW);
GPIO_Init(DATA_GPIO,DATA_PIN,GPIO_MODE_OUT_PP_LOW_SLOW);

max7219_posli(DECODE_MODE, DECODE_ALL);
max7219_posli(SCAN_LIMIT, 7);
max7219_posli(INTENSITY, 5);
max7219_posli(DISPLAY_TEST, DISPLAY_TEST_OFF);
max7219_posli(SHUTDOWN, DISPLAY_ON);
}

void max7219_posli(uint8_t adresa, uint8_t data){
uint8_t maska;
CS_LOW;

maska = 0b10000000;
CLK_LOW;
while(maska){
	if(maska & adresa){
		DATA_HIGH;
	}
	else{
		DATA_LOW;
	}
	CLK_HIGH;
	maska = maska>>1;
	CLK_LOW;
}

maska = 0b10000000;
while(maska){
	if(maska & data){
		DATA_HIGH;
	}
	else{ 
		DATA_LOW;
	}
	CLK_HIGH;
	maska = maska>>1;
	CLK_LOW;
}
CS_HIGH;
}
